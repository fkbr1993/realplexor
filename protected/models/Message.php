<?php


/**
 * This is the model class for table "message".
 *
 * The followings are the available columns in table 'message':
 * @property integer $id
 * @property integer $from_id
 * @property integer $to_id
 * @property string $date
 * @property integer $read_state
 * @property string $body
 * @property integer $from_deleted
 * @property integer $to_deleted
 *
 * The followings are the available model relations:
 * @property User $from
 * @property User $to
 */
class Message extends CActiveRecord
{
	const STATE_READ = 2;
	const STATE_GOT = 1;
	const STATE_NOT_GOT = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('from_id, to_id, read_state, body', 'required'),
			array('from_id, to_id, read_state, from_deleted, to_deleted', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, from_id, to_id, date, read_state, body, from_deleted, to_deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'from' => array(self::BELONGS_TO, 'User', 'from_id'),
			'to' => array(self::BELONGS_TO, 'User', 'to_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'from_id' => 'From',
			'to_id' => 'To',
			'date' => 'Date',
			'read_state' => 'Read State',
			'body' => 'Body',
			'from_deleted' => 'From Deleted',
			'to_deleted' => 'To Deleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('from_id',$this->from_id);
		$criteria->compare('to_id',$this->to_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('read_state',$this->read_state);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('from_deleted',$this->from_deleted);
		$criteria->compare('to_deleted',$this->to_deleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Message the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @param $uid
	 *
	 * @return static
	 */
	public static function findNew($uid){
		$hs = new HandlerSocket('localhost', 9999);
		//open index and init work with columns
		$hs->openIndex(1, 'pincat', 'message', 'message_id', 'to_id,read_state,date,body,from_id', 'date,id');
		$retval = $hs->executeSingle(1, '=', [$uid, self::STATE_NOT_GOT], 1, 0,null,null,['F', '=>', 0, time()-6]);
		if (!empty($retval)){
            $readstate = self::STATE_GOT;
            if (!empty($_GET['chatting']))
                if ($_GET['chatting'] == $retval[4])
	                $readstate = self::STATE_READ;
            $hs->executeSingle(1, '=', array(''), 1, 0, 'U', array($readstate), ['F', '=>', 0, time()-6]);

            return array(
                'body' => $retval[3],
                'from' => $retval[4],
                'time' => $retval[2],
                'photo' => $message->from->getPhoto200(),
                'name' => $message->from->getFullName(),
            );
		}
		return null;
	}

	/**
	 * @param $from
	 * @param $body
	 * @param $to
	 * @param $time
	 *
	 * @return bool
	 */
	public static function create($from, $body, $to, $time){
		$message = new self();
		$message->from_id = $from;
		$message->to_id = $to;
        if ($from == $to)
            $message->read_state = self::STATE_READ;
		$message->read_state = self::STATE_NOT_GOT;
		$message->body = $body;
        $message->date = floor($time/1000 + 0.5);
		if ($message->save(false))
			return true;
		return false;
	}
}
