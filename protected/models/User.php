<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $email
 * @property string $hash
 * @property string $first_name
 * @property string $last_name
 * @property string $about
 * @property integer $sex
 * @property string $bdate
 * @property string $country
 * @property string $photo_50
 * @property string $photo_200
 * @property string $photo_85
 * @property string $city
 * @property string $admin_comment
 * @property string $skype
 * @property string $phone
 * @property string $role
 * @property integer $email_confirmed
 * @property string $last_seen
 * @property integer $active
 * @property integer $banned
 * @property integer $deleted
 * @property string $network
 * @property string $login
 * @property string $valid
 * @property string $restore_link
 *
 * The followings are the available model relations:
 * @property Comment[] $comments
 * @property Comment[] $comments1
 * @property Follow[] $follows
 * @property Follow[] $follows1
 * @property Like[] $likes
 * @property Message[] $messages
 * @property Message[] $messages1
 * @property Notification[] $notifications
 * @property Photo[] $photos
 * @property PhotoLike[] $photoLikes
 * @property Post[] $posts
 * @property PostComment[] $postComments
 * @property Promo[] $promos
 * @property PromoLike[] $promoLikes
 * @property PromoPhoto[] $promoPhotos
 * @property RequestGroup[] $requestGroups
 * @property Community[] $communities
 * @property SocialAccount[] $socialAccounts
 * @property Spam[] $spams
 * @property Topic[] $topics
 * @property TopicComment[] $topicComments
 * @property Video[] $videos
 * @property VideoComment[] $videoComments
 * @property VideoLike[] $videoLikes
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sex, photo_50, photo_200, photo_85, email_confirmed, last_seen', 'required'),
			array('sex, email_confirmed, active, banned, deleted', 'numerical', 'integerOnly'=>true),
			array('email, hash, first_name, last_name, about, country, photo_50, photo_200, photo_85, city, admin_comment, skype, phone, role, restore_link', 'length', 'max'=>255),
			array('last_seen', 'length', 'max'=>10),
			array('network, login, valid', 'length', 'max'=>111),
			array('bdate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, hash, first_name, last_name, about, sex, bdate, country, photo_50, photo_200, photo_85, city, admin_comment, skype, phone, role, email_confirmed, last_seen, active, banned, deleted, network, login, valid, restore_link', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'comments' => array(self::HAS_MANY, 'Comment', 'user_id'),
			'comments1' => array(self::HAS_MANY, 'Comment', 'user_id'),
			'follows' => array(self::HAS_MANY, 'Follow', 'followed_id'),
			'follows1' => array(self::HAS_MANY, 'Follow', 'follower_id'),
			'likes' => array(self::HAS_MANY, 'Like', 'user_id'),
			'messages' => array(self::HAS_MANY, 'Message', 'from_id'),
			'messages1' => array(self::HAS_MANY, 'Message', 'to_id'),
			'notifications' => array(self::HAS_MANY, 'Notification', 'user_id'),
			'photos' => array(self::HAS_MANY, 'Photo', 'user_id'),
			'photoLikes' => array(self::HAS_MANY, 'PhotoLike', 'user_id'),
			'posts' => array(self::HAS_MANY, 'Post', 'user_id'),
			'postComments' => array(self::HAS_MANY, 'PostComment', 'user_id'),
			'promos' => array(self::HAS_MANY, 'Promo', 'user_id'),
			'promoLikes' => array(self::HAS_MANY, 'PromoLike', 'user_id'),
			'promoPhotos' => array(self::HAS_MANY, 'PromoPhoto', 'user_id'),
			'requestGroups' => array(self::HAS_MANY, 'RequestGroup', 'user_id'),
			'communities' => array(self::MANY_MANY, 'Community', 'role(user_id, community_id)'),
			'socialAccounts' => array(self::HAS_MANY, 'SocialAccount', 'user_id'),
			'spams' => array(self::HAS_MANY, 'Spam', 'user_id'),
			'topics' => array(self::HAS_MANY, 'Topic', 'user_id'),
			'topicComments' => array(self::HAS_MANY, 'TopicComment', 'user_id'),
			'videos' => array(self::HAS_MANY, 'Video', 'user_id'),
			'videoComments' => array(self::HAS_MANY, 'VideoComment', 'user_id'),
			'videoLikes' => array(self::HAS_MANY, 'VideoLike', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'hash' => 'Hash',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'about' => 'About',
			'sex' => 'Sex',
			'bdate' => 'Bdate',
			'country' => 'Country',
			'photo_50' => 'Photo 50',
			'photo_200' => 'Photo 200',
			'photo_85' => 'Photo 85',
			'city' => 'City',
			'admin_comment' => 'Admin Comment',
			'skype' => 'Skype',
			'phone' => 'Phone',
			'role' => 'Role',
			'email_confirmed' => 'Email Confirmed',
			'last_seen' => 'Last Seen',
			'active' => 'Active',
			'banned' => 'Banned',
			'deleted' => 'Deleted',
			'network' => 'Network',
			'login' => 'Login',
			'valid' => 'Valid',
			'restore_link' => 'Restore Link',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('hash',$this->hash,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('about',$this->about,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('bdate',$this->bdate,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('photo_50',$this->photo_50,true);
		$criteria->compare('photo_200',$this->photo_200,true);
		$criteria->compare('photo_85',$this->photo_85,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('admin_comment',$this->admin_comment,true);
		$criteria->compare('skype',$this->skype,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('email_confirmed',$this->email_confirmed);
		$criteria->compare('last_seen',$this->last_seen,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('banned',$this->banned);
		$criteria->compare('deleted',$this->deleted);
		$criteria->compare('network',$this->network,true);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('valid',$this->valid,true);
		$criteria->compare('restore_link',$this->restore_link,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return string
     */
    public function getPhoto200(){
        return !empty($this->photo_200) ? $this->photo_200 : '/images/ava200.jpg';
    }

    /**
     * Возвращает полное имя.
     * @return string
     */
    public function getFullName()
    {
        return trim($this->first_name . ' ' . $this->last_name);
    }

    /**
     * Update user last seen in chat
     * @param $users
     * @return array
     */
    public static function getIsOnline($users){
        $isOnline = array();
        foreach($users as $uid){
            $lastSeen = self::model()->findByPk($uid)->last_seen;
            if ($lastSeen > (time()-5*60)){
                $isOnline[$uid] = 'online';
            } else {
                $datetime = new \DateTime();
                $datetime->setTimestamp($lastSeen);
                $timeAgo = $datetime->format('c');
                $isOnline[$uid] = $timeAgo;
            }
        }
        return $isOnline;
    }
}
