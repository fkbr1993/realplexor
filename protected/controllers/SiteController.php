<?php

class SiteController extends Controller
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->redirect('http://www.pincat.ru');
	}

	public function actionError()
	{
		CVarDumper::dump(Yii::app()->errorHandler->error, 3, 1, 1);
	}

	/**
	 * Same as actionPoll except of this action serves long-polling request.
	 * Maximum polling time is min(30sec, max_execution_time)/2
	 */
	public function actionLongPoll()
	{
		Message::findNew(1);


		$limit = min(30, ini_get('max_execution_time')) / 2;
		$start = time();
		if (!empty($_GET['userId'])) {
			$uid = $_GET['userId'];
			for ($i = 0; ($i == 0) || (time() - $start < $limit); $i++) {
				$data = array(
					'result' => 'success',
					'message' => Message::findNew($uid),
					'type' => 'poll',
				);
				if (!empty($data['message']) || (time() - $start >= $limit)) {
					break;
				}
				usleep(750 * 1000);
			}
            if (empty($data['message'])){//if no messages take who is online
                if (!empty($_GET['getIsOnline'])){//if in chat
                    $data['online'] = User::getIsOnline($_GET['getIsOnline']);
                    $data['type'] = 'online';
                }
            }
			header("Content-Type: text/javascript");
			print 'parseJSONP(' . CJSON::encode($data) . ');';
		}
	}

	/**
	 * Posts new message to session.
	 */
	public function actionPost()
	{
        if (!empty($_GET['from']) && !empty($_GET['message']) && !empty($_GET['to'])) {
			if (Message::create($_GET['from'], $_GET['message'], $_GET['to'], $_GET['time'])) {
				$data = array(
					'type' => 'post',
                    'to' => $_GET['to'],
                    'result' => 'success',
                    'time' => $_GET['time'],
                );
			} else {
				$data = array(
                    'type' => 'post',
					'result' => 'error',
                    'time' => $_GET['time'],
                );
			}
            $user = User::model()->findByPk($_GET['from']);
            $user->last_seen = time();
            $user->save(false);
			header("Content-Type: text/javascript");
			print 'parseJSONP(' . CJSON::encode($data) . ');';
		}
	}
}
