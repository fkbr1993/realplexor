<?php
/**
 * Author: alfred
 * Email: alfred
 */
return array(
	'components' => array(
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=pincat',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'mysql',
			'charset' => 'utf8',
			'tablePrefix' => false,
			'schemaCachingDuration' => 0,
			'enableProfiling' => true,
			'enableParamLogging' => true,
		),
	),
	'params' => array(
		'debug' => true,
	),
);
